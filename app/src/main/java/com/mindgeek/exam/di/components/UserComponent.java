package com.mindgeek.exam.di.components;

import com.mindgeek.exam.di.PerActivity;
import com.mindgeek.exam.di.modules.ActivityModule;
import com.mindgeek.exam.di.modules.UserModule;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class, UserModule.class})
public interface UserComponent {

}
