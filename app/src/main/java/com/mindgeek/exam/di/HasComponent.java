package com.mindgeek.exam.di;

public interface HasComponent<T> {

    T getComponent();
}
