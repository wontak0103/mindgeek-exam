package com.mindgeek.exam.di.components;

import android.app.Activity;

import com.mindgeek.exam.di.PerActivity;
import com.mindgeek.exam.di.modules.ActivityModule;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    Activity activity();
}
