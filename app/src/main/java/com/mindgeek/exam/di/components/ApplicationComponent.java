package com.mindgeek.exam.di.components;

import com.mindgeek.exam.base.BaseActivity;
import com.mindgeek.exam.di.modules.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(BaseActivity activity);
}
