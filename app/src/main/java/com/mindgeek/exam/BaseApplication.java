package com.mindgeek.exam;

import android.app.Application;

import com.mindgeek.exam.di.components.ApplicationComponent;
import com.mindgeek.exam.di.components.DaggerApplicationComponent;
import com.mindgeek.exam.di.modules.ApplicationModule;

public class BaseApplication extends Application {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        initializeInjector();
    }

    private void initializeInjector() {
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
