package com.mindgeek.exam.presentation.ui.activities;

import android.content.Intent;
import android.os.Bundle;

import com.mindgeek.exam.R;
import com.mindgeek.exam.base.BaseActivity;
import com.mindgeek.exam.presentation.ui.fragments.MainFragment;
import com.mindgeek.exam.utils.SharedPreferencesUtil;

public class MainActivity extends BaseActivity {

    public static final String KEY_UNLOCKED = "unlocked";

    private MainFragment fragment;
    private boolean unlocked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        unlocked = getIntent().getBooleanExtra(KEY_UNLOCKED, false);

        initializeFragment();
    }

    private void initializeFragment() {
        fragment = MainFragment.newInstance();
        addFragment(R.id.content_main, fragment);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (SharedPreferencesUtil.getSecureModeActivate(this) && !unlocked) {
            launchLockActivity();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        unlocked = false;
    }

    public void launchLockActivity() {
        Intent intent = new Intent(this, LockActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
