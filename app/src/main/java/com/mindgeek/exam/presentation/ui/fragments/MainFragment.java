package com.mindgeek.exam.presentation.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Switch;

import com.jakewharton.rxbinding2.widget.RxCheckedTextView;
import com.jakewharton.rxbinding2.widget.RxCompoundButton;
import com.jakewharton.rxbinding2.widget.RxTextView;
import com.mindgeek.exam.R;
import com.mindgeek.exam.base.BaseFragment;
import com.mindgeek.exam.utils.SharedPreferencesUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.Unbinder;
import timber.log.Timber;

public class MainFragment extends BaseFragment {

    @BindView(R.id.toggle_secure_mode_activate)
    Switch secureModeActivateToggle;

    @BindView(R.id.input_password)
    EditText passwordInput;

    private Unbinder unbinder;

    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();

        Bundle args = new Bundle();
        fragment.setArguments(args);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        unbinder = ButterKnife.bind(this, view);

        secureModeActivateToggle.setChecked(SharedPreferencesUtil.getSecureModeActivate(getContext()));
        RxCompoundButton.checkedChanges(secureModeActivateToggle)
                .skip(1)
                .subscribe(isChecked -> {
                    SharedPreferencesUtil.setSecureModeActivate(getContext(), isChecked);
                    if (isChecked) {
                        SharedPreferencesUtil.setSecurePassword(getContext(), passwordInput.getText().toString());
                    }
                });

        passwordInput.setText(SharedPreferencesUtil.getSecurePassword(getContext()));
        RxTextView.afterTextChangeEvents(passwordInput)
                .skip(1)
                .subscribe(event -> SharedPreferencesUtil.setSecurePassword(getContext(), event.editable().toString()));

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
