package com.mindgeek.exam.presentation.ui.activities;

import android.content.Intent;
import android.os.Bundle;

import com.mindgeek.exam.R;
import com.mindgeek.exam.base.BaseActivity;
import com.mindgeek.exam.presentation.ui.fragments.LockFragment;

public class LockActivity extends BaseActivity {

    private LockFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lock);

        initializeFragment();
    }

    private void initializeFragment() {
        fragment = LockFragment.newInstance();
        addFragment(R.id.content_main, fragment);
    }

    public void launchMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(MainActivity.KEY_UNLOCKED, true);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
