package com.mindgeek.exam.presentation.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.support.test.espresso.intent.Intents;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.mindgeek.exam.R;
import com.mindgeek.exam.presentation.ui.utils.actions.CheckedViewAction;
import com.mindgeek.exam.utils.SharedPreferencesUtil;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.InstrumentationRegistry.getTargetContext;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.isChecked;
import static android.support.test.espresso.matcher.ViewMatchers.isNotChecked;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class MainActivityTest {

    private static final String FAKE_PASSWORD = "1234";

    private Context context;

    @Rule
    public ActivityTestRule<MainActivity> activityTestRule = new ActivityTestRule(MainActivity.class, true, false);

    @Before
    public void setUp() {
        Intents.init();
        context = getInstrumentation().getTargetContext();
    }

    @After
    public void tearDown() {
        Intents.release();
    }

    @Test
    public void secureModeActivated() {
        SharedPreferencesUtil.setSecureModeActivate(context, true);
        Intent intent = new Intent();
        intent.putExtra(MainActivity.KEY_UNLOCKED, true);
        activityTestRule.launchActivity(intent);

        onView(withId(R.id.toggle_secure_mode_activate))
                .check(matches(isChecked()));
    }

    @Test
    public void secureModeInactivated() {
        SharedPreferencesUtil.setSecureModeActivate(context, false);
        Intent intent = new Intent();
        intent.putExtra(MainActivity.KEY_UNLOCKED, true);
        activityTestRule.launchActivity(intent);

        onView(withId(R.id.toggle_secure_mode_activate))
                .check(matches(isNotChecked()));
    }

    @Test
    public void shownLockScreenWhenSecureModeActivated() {
        SharedPreferencesUtil.setSecureModeActivate(getTargetContext(), true);
        activityTestRule.launchActivity(new Intent());

        intended(hasComponent(LockActivity.class.getName()));
    }

    @Test
    public void notShownLockScreenWhenSecureModeInactivated() {
        SharedPreferencesUtil.setSecureModeActivate(getTargetContext(), false);
        activityTestRule.launchActivity(new Intent());

        intended(hasComponent(MainActivity.class.getName()));
    }

    @Test
    public void setSecureModeActivate() {
        SharedPreferencesUtil.setSecureModeActivate(context, false);
        activityTestRule.launchActivity(new Intent());

        onView(withId(R.id.input_password))
                .perform(typeText(FAKE_PASSWORD));
        onView(withId(R.id.toggle_secure_mode_activate))
                .perform(new CheckedViewAction(true));

        assertEquals(SharedPreferencesUtil.getSecurePassword(getTargetContext()), FAKE_PASSWORD);
    }
}