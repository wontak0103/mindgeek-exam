package com.mindgeek.exam.presentation.ui.utils.actions;

import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.view.View;
import android.widget.Checkable;

import org.hamcrest.Description;
import org.hamcrest.Matcher;

import static org.hamcrest.core.Is.isA;

public class CheckedViewAction implements ViewAction {

    private boolean isChecked;

    public CheckedViewAction(boolean isChecked) {
        this.isChecked = isChecked;
    }

    @Override
    public Matcher<View> getConstraints() {
        return new Matcher<View>() {
            @Override
            public boolean matches(Object item) {
                return isA(Checkable.class).matches(item);
            }

            @Override
            public void describeMismatch(Object item, Description description) {}

            @Override
            public void _dont_implement_Matcher___instead_extend_BaseMatcher_() {}

            @Override
            public void describeTo(Description description) {}
        };
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public void perform(UiController uiController, View view) {
        Checkable checkable = (Checkable) view;
        checkable.setChecked(isChecked);
    }
}
