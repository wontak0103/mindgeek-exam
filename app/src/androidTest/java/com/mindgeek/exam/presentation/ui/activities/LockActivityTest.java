package com.mindgeek.exam.presentation.ui.activities;

import android.support.test.espresso.intent.Intents;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.mindgeek.exam.R;
import com.mindgeek.exam.utils.SharedPreferencesUtil;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.locks.Lock;

import static android.support.test.InstrumentationRegistry.getTargetContext;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class LockActivityTest {

    private static final String FAKE_PASSWORD = "1234";

    @Rule
    public ActivityTestRule<LockActivity> activityTestRule = new ActivityTestRule(LockActivity.class);

    @Before
    public void setUp() {
        Intents.init();
    }

    @After
    public void tearDown() {
        Intents.release();
    }

    @Test
    public void shownMainScreenWhenCorrectPassword() {
        SharedPreferencesUtil.setSecurePassword(getTargetContext(), FAKE_PASSWORD);

        onView(withId(R.id.input_password))
                .perform(typeText(FAKE_PASSWORD), closeSoftKeyboard());
        onView(withId(R.id.button_unlock))
                .perform(click());

        intended(hasComponent(MainActivity.class.getName()));
    }

    @Test
    public void errorShownOnIncorrectPassword() {
        SharedPreferencesUtil.setSecurePassword(getTargetContext(), FAKE_PASSWORD);

        onView(withId(R.id.input_password))
                .perform(typeText(""), closeSoftKeyboard());
        onView(withId(R.id.button_unlock))
                .perform(click());

        onView(withText(R.string.message_incorrect_password))
                .inRoot(withDecorView(not(is(activityTestRule.getActivity().getWindow().getDecorView()))))
                .check(matches(isDisplayed()));
    }
}