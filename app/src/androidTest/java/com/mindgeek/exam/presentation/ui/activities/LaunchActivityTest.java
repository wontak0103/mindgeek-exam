package com.mindgeek.exam.presentation.ui.activities;

import android.content.Intent;
import android.support.test.espresso.intent.Intents;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.mindgeek.exam.utils.SharedPreferencesUtil;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.InstrumentationRegistry.getTargetContext;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class LaunchActivityTest {

    @Rule
    public ActivityTestRule<LaunchActivity> activityTestRule = new ActivityTestRule(LaunchActivity.class, true, false);

    @Before
    public void setUp() {
        Intents.init();
    }

    @After
    public void tearDown() {
        Intents.release();
    }

    @Test
    public void secureModeActivated() {
        SharedPreferencesUtil.setSecureModeActivate(getTargetContext(), true);
        activityTestRule.launchActivity(new Intent());

        intended(hasComponent(LockActivity.class.getName()));
    }

    @Test
    public void secureModeInactivated() {
        SharedPreferencesUtil.setSecureModeActivate(getTargetContext(), false);
        activityTestRule.launchActivity(new Intent());

        intended(hasComponent(MainActivity.class.getName()));
    }
}